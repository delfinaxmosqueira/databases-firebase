
const firebaseConfig = {
    apiKey: "AIzaSyDmnF932xBaELmmm7E3IjlqqPMoNoqABpA",
    authDomain: "controlacceso-mosqueira.firebaseapp.com",
    dataBaseUR: "https://controlacceso-mosqueira-default-rtdb.firebaseio.com",
    projectId: "controlacceso-mosqueira",
    storageBucket: "controlacceso-mosqueira.appspot.com",
    messagingSenderId: "78773256530",
    appId: "1:78773256530:web:bfa090beebf8ec4e457a8b"
  };

  firebase.initializeApp(firebaseConfig);

  const db = firebase.database();

  const username = prompt("Please Tell Us Your Name");

  document.getElementById("message-form").addEventListener("submit", sendMessage);

  function sendMessage(e) {
    e.preventDefault();
  
    // get values to be submitted
    const timestamp = Date.now();
    const messageInput = document.getElementById("message-input");
    const message = messageInput.value;
  
    // clear the input box
    messageInput.value = "";
  
    //auto scroll to bottom
    document
      .getElementById("messages")
      .scrollIntoView({ behavior: "smooth", block: "end", inline: "nearest" });
  
    // create db collection and send in the data
    db.ref("messages/" + timestamp).set({
      username,
      message,
    });
  }
  const fetchChat = db.ref("messages/");

  fetchChat.on("child_added", function (snapshot) {
    const messages = snapshot.val();
    const message = `<li class=${
      username === messages.username ? "sent" : "receive"
    }><span>${messages.username}: </span>${messages.message}</li>`;
    // append the message on the page
    document.getElementById("messages").innerHTML += message;
  });